#ifndef SHELL_H
#define SHELL_H

#include "pico/stdlib.h"

/* ---------- Global Variables ---------- */
// Flag for end of command reception
extern bool UART_RX_DONE_FLAG;

// Flag for display mode
extern uint8_t DISPLAY_MODE;


/* ---------- Global Functions ---------- */
void shell__init();
void shell__on_uart_rx();
void shell__process_command();



#endif