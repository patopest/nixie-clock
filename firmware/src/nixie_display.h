#ifndef NIXIE_DISPLAY_H
#define NIXIE_DISPLAY_H

/* ---------- Global Defines ---------- */
#define DIGIT_OFF     0xFF


/* ---------- Global Functions ---------- */
void nixie_display__init();
void nixie_display__set_digit(uint8_t display, uint8_t digit);
void nixie_display__write_digits();
bool nixie_display__trigger_sequence();

#endif