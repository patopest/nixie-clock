#include <stdio.h>
#include "pico/stdlib.h"
#include "tusb.h"

#include "usb.h"
#include "midi.h"


/* --------- Local Defines --------- */
/* A combination of interfaces must have a unique product id, since PC will save device driver after the first plug.
 * Same VID/PID with different interface e.g MSC (first), then CDC (later) will possibly cause system error on PC.
 *
 * Auto ProductID layout's Bitmap:
 *   [MSB]       MIDI | HID | MSC | CDC          [LSB]
 */
#define _PID_MAP(itf, n)  ( (CFG_TUD_##itf) << (n) )
#define USB_PID           (0x4000 | _PID_MAP(CDC, 0) | _PID_MAP(MSC, 1) | _PID_MAP(HID, 2) | \
                           _PID_MAP(MIDI, 3) | _PID_MAP(VENDOR, 4) )

#define USB_DESC_TOTAL_LEN      (TUD_CONFIG_DESC_LEN + TUD_MIDI_DESC_LEN)

#define USB_MANUFACTURER_NAME   "Patopest"
#define USB_DEVICE_NAME         "Nixie Clock"
#define USB_CDC_NAME            USB_DEVICE_NAME " CDC"
#define USB_MIDI_NAME           USB_DEVICE_NAME " MIDI"


/* --------- Local Declarations --------- */
enum {
    // Interface numbers
    ITF_NUM_MIDI = 0,
    ITF_NUM_MIDI_STREAMING,
    ITF_NUM_TOTAL
};

enum {
    // Available USB Endpoints: 5 IN/OUT EPs and 1 IN EP
    EP_EMPTY = 0,
    EPNUM_0_MIDI,
    // EPNUM_1_CDC_NOTIF,
    // EPNUM_1_CDC,
};


/* --------- Local Variables --------- */
static const tusb_desc_device_t device_descriptor = {
    .bLength            = sizeof(device_descriptor),    // Size of this descriptor in bytes.
    .bDescriptorType    = TUSB_DESC_DEVICE,             // DEVICE Descriptor Type.
    .bcdUSB             = 0x0110,                       // BUSB Specification Release Number in Binary-Coded Decimal (i.e., 2.10 is 210H).

    .bDeviceClass       = TUSB_CLASS_MISC,              // Class code (assigned by the USB-IF).
    .bDeviceSubClass    = MISC_SUBCLASS_COMMON,         // Subclass code (assigned by the USB-IF).
    .bDeviceProtocol    = MISC_PROTOCOL_IAD,            // Protocol code (assigned by the USB-IF).
    .bMaxPacketSize0    = CFG_TUD_ENDPOINT0_SIZE,       // Maximum packet size for endpoint zero (only 8, 16, 32, or 64 are valid). For HS devices is fixed to 64.

    .idVendor           = 0xCafe,                       // Vendor ID (assigned by the USB-IF).
    .idProduct          = USB_PID,                      // Product ID (assigned by the manufacturer).
    .bcdDevice          = 0x0100,                       // Device release number in binary-coded decimal.

    .iManufacturer      = 0x01,                         // Index of string descriptor describing manufacturer.
    .iProduct           = 0x02,                         // Index of string descriptor describing product.
    .iSerialNumber      = 0x03,                         // Index of string descriptor describing the device's serial number.

    .bNumConfigurations = 0x01                          // Number of possible configurations.
};

// array of pointer to string descriptors
static const char* string_descriptor[] = {
    (const char[]){0x09, 0x04},                 // 0: supported language is English (0x0409)
    USB_MANUFACTURER_NAME,                      // 1: Manufacturer
    USB_DEVICE_NAME,                            // 2: Product
    "123456",                                   // 3: Serials, should use chip ID
    USB_CDC_NAME,                               // 4: CDC Interface
    USB_MIDI_NAME                               // 5: MIDI
};
static uint16_t desc_str[32];

static const uint8_t configuration_descriptor[] = {
    // Config number, interface count, string index, total length, attribute, power in mA
    TUD_CONFIG_DESCRIPTOR(1, ITF_NUM_TOTAL, 0, USB_DESC_TOTAL_LEN, 0x00, 0),

    // Interface number, string index, EP Out & EP In address, EP size
    TUD_MIDI_DESCRIPTOR(ITF_NUM_MIDI, 5, EPNUM_0_MIDI, (0x80 | EPNUM_0_MIDI), 64)
};



void usb__init() {

    printf("Init USB module\n");

    tusb_init();
}


void usb__run() {
    midi_packet_t packet;
    uint32_t len;

    tud_task();

    // Receiving midi
    while (len = tud_midi_available()) {
        if (len > sizeof(midi_packet_u)) {
            len = sizeof(midi_packet_u);    // avoid overreading
        }
        tud_midi_stream_read(&(packet.data), len);
        midi__handle_packet(packet);
    }

    // Sending midi
    while (midi__available_packet()) {
        midi__read_packet(&packet);
        tud_midi_stream_write(0, (uint8_t *) &(packet.data), packet.length);
    }
}



/* --------- TinyUSB callbacks --------- */
// Invoked when received GET DEVICE DESCRIPTOR
// Application return pointer to descriptor
const uint8_t* tud_descriptor_device_cb(void) {
    printf("tud_descriptor_device_cb\n");
    return (const uint8_t*) &device_descriptor;
}


// Invoked when received GET CONFIGURATION DESCRIPTOR
// Application return pointer to descriptor
// Descriptor contents must exist long enough for transfer to complete
const uint8_t* tud_descriptor_configuration_cb(uint8_t index) {
    (void) index; // for multiple configurations

    printf("tud_descriptor_configuration_cb\n");
    return configuration_descriptor;
}


// Invoked when received GET STRING DESCRIPTOR request
// Application return pointer to descriptor, whose contents must exist long enough for transfer to complete
const uint16_t* tud_descriptor_string_cb(uint8_t index, uint16_t langid) {
    (void) langid;

    uint8_t chr_count;

    if ( index == 0) {
        memcpy(&desc_str[1], string_descriptor[0], 2);
        chr_count = 1;
    }
    else {
        // Note: the 0xEE index string is a Microsoft OS 1.0 Descriptors.
        // https://docs.microsoft.com/en-us/windows-hardware/drivers/usbcon/microsoft-defined-usb-descriptors

        if ( !(index < sizeof(string_descriptor)/sizeof(string_descriptor[0])) ) return NULL;

        const char* str = string_descriptor[index];

        // Cap at max char
        chr_count = (uint8_t) strlen(str);
        if ( chr_count > 31 ) chr_count = 31;

        // Convert ASCII string into UTF-16
        for(uint8_t i=0; i<chr_count; i++) {
            desc_str[1+i] = str[i];
        }
    }

    // first byte is length (including header), second byte is string type
    desc_str[0] = (uint16_t) ((TUSB_DESC_STRING << 8 ) | (2*chr_count + 2));

    return desc_str;
}

