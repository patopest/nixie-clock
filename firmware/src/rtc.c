#include <stdio.h>
#include "pico/stdlib.h"
#include "pico/binary_info.h"
#include "hardware/i2c.h"
#include "hardware/rtc.h"
#include "pico/util/datetime.h"

#include "rtc.h"
#include "config.h"
#include "eeprom.h"


/* ---------- Local Defines ---------- */
// I2C DS3231 module
#define DS3231_ADDR             0x68

#define SECONDS_REG             0x00
#define MINUTES_REG             0x01
#define HOURS_REG               0x02
#define DAY_REG                 0x03
#define DATE_REG                0x04
#define MONTH_REG               0x05
#define YEAR_REG                0x06

#define CONTROL_REG             0x0E
#define STATUS_REG              0x0F
#define AGING_REG               0x10
#define TEMP_MSB_REG            0x11
#define TEMP_LSB_REG            0x12


/* ---------- Global Variables ---------- */
char *days[7] = {"Mon","Tue","Wed","Thr","Fri","Sat","Sun"};

/* ---------- Local Variables ---------- */
static uint8_t mode = RTC_MODE_I2C;


/* ---------- Local Functions ---------- */
static void ds3231_read(uint8_t *buffer, uint8_t length, uint8_t start_reg);
static void ds3231_write(uint8_t *buffer, uint8_t length, uint8_t start_reg);
static void convert_to_hex(uint8_t *buffer, uint8_t length);
static void convert_to_decimal(uint8_t *buffer, uint8_t length);
static void convert_to_time(datetime_t *datetime, rtc_time_t *time);
static void convert_to_datetime(rtc_time_t *time, datetime_t *datetime);



void rtc__init() {

    printf("Init I2C RTC module\n");

    i2c_init(RTC_I2C, RTC_I2C_FREQ * 1000);
    gpio_set_function(RTC_I2C_SDA_PIN, GPIO_FUNC_I2C);
    gpio_set_function(RTC_I2C_SCL_PIN, GPIO_FUNC_I2C);
    gpio_pull_up(RTC_I2C_SDA_PIN);
    gpio_pull_up(RTC_I2C_SCL_PIN);
    // Make the I2C pins available to picotool
    bi_decl(bi_2pins_with_func(RTC_I2C_SDA_PIN, RTC_I2C_SCL_PIN, GPIO_FUNC_I2C));
    bi_decl(bi_2pins_with_names(RTC_I2C_SDA_PIN, "DS3231 RTC module", RTC_I2C_SCL_PIN, "DS3231 RTC module"));


    // read control register
    uint8_t buffer;
    ds3231_read(&buffer, 1, STATUS_REG);
    printf("Control reg value: 0x%x\n", buffer);

    if (buffer != 0) { // if control buffer is not properly set up
        printf("Writing control register\n");
        buffer = 0x00;
        ds3231_write(&buffer, 1, STATUS_REG);
    }

    printf("Init Pico RTC module\n");
    rtc_init();
}


void rtc__get_config() {

    uint8_t value[5];
    ds3231_read(value, 5, CONTROL_REG);
    printf("DS3231 Config:\n");
    printf("\tcontrol reg: 0x%x\n", value[0]);
    printf("\tstatus reg: 0x%x\n", value[1]);
    printf("\taging reg: %d\n", value[2]);
    uint8_t temp_decimals = (value[4] >> 6) * 25;
    printf("\ttemp: %d.%d°C\n", (int8_t) value[3], temp_decimals);
}


void rtc__set_mode(uint8_t rtc_mode) {
    mode = rtc_mode;
}


void rtc__read_time(rtc_time_t *time, uint8_t length) {

    if (mode == RTC_MODE_I2C) {
        ds3231_read(time->array, length, SECONDS_REG);
        convert_to_decimal(time->array, length);
    }
    else {
        datetime_t t;
        rtc_get_datetime(&t);
        convert_to_time(&t, time);
    }
}


static void ds3231_read(uint8_t *buffer, uint8_t length, uint8_t start_reg) {

    i2c_write_blocking(RTC_I2C, DS3231_ADDR, &start_reg, 1, true);
    i2c_read_blocking(RTC_I2C, DS3231_ADDR, buffer, length, false);
}


void rtc__write_time(rtc_time_t *time, uint8_t length) {

    if (mode == RTC_MODE_I2C) {
        eeprom__write_time(time, length);

        convert_to_hex(time->array, length);
        ds3231_write(time->array, length, SECONDS_REG);
    }
    else {
        datetime_t t;
        convert_to_datetime(time, &t);
        rtc_set_datetime(&t);
    }
}


static void ds3231_write(uint8_t *buffer, uint8_t length, uint8_t start_reg) {

    uint8_t buf[2];

    for (uint8_t i = 0; i < length; i++) {
        buf[0] = start_reg + i;
        buf[1] = buffer[i];
        i2c_write_blocking(RTC_I2C, DS3231_ADDR, buf, 2, false);
    }
}


void rtc__set_calibration(int8_t value) {

    uint8_t status = 0x08;
    ds3231_write(&status, 1, STATUS_REG);

    ds3231_write(&value, 1, AGING_REG);

    // trigger manual conversion
    uint8_t control = 0x24;
    ds3231_write(&control, 1, CONTROL_REG);

    uint8_t ret = 0;
    ds3231_read(&ret, 1, AGING_REG);
    printf("aging: %d\n", (int8_t) ret);
}


// converts a buffer containing time values in decimal to hex (for DS3231 registers)
static void convert_to_hex(uint8_t *buffer, uint8_t length) {

    for (uint8_t i = 0; i < length; i++) {
        uint8_t tens = buffer[i] / 10;
        uint8_t units = buffer[i] % 10;
        
        // printf("buffer %d = %d%d\n", buffer[i], tens, units);
        buffer[i] = units;
        buffer[i] |= (tens << 4);
    }
}


// converts a buffer of values in hex (ds3231 registers) to decimal
static void convert_to_decimal(uint8_t *buffer, uint8_t length) {

    for (uint8_t i = 0; i < length; i++) {
        uint8_t tens = buffer[i] >> 4;
        uint8_t units = buffer[i] & 0x0F;

        // printf("buffer 0x%x = %d%d\n", buffer[i], tens, units);
        buffer[i] = units + (tens * 10);
    }
}


// convert from pico's datetime struct to our timecode struct
static void convert_to_time(datetime_t *datetime, rtc_time_t *time) {
    time->year      = datetime->year;
    time->month     = datetime->month;
    time->date      = datetime->day;
    time->hours     = datetime->hour;
    time->minutes   = datetime->min;
    time->seconds   = datetime->sec;

    // datertc_time_t starts on Sunday, we start on Monday
    if (datetime->dotw == 0) {
        time->day = 7;
    }
    else {
        time->day = datetime->dotw;
    }
}


// convert from timecode struct to pico's datetime struct
static void convert_to_datetime(rtc_time_t *time, datetime_t *datetime) {
    datetime->year      = time->year;
    datetime->month     = time->month;
    datetime->day       = time->date;
    datetime->hour      = time->hours;
    datetime->min       = time->minutes;
    datetime->sec       = time->seconds;

    // datertc_time_t starts on Sunday, we start on Monday
    if (time->day == 7) {
        datetime->dotw = 0;
    }
    else {
        datetime->dotw = time->day;
    }
}


/*   Prints   */

void rtc__print_datetime(rtc_time_t *time) {

    printf("%s %.2d-%.2d-20%d %.2d:%.2d:%.2d\n", 
        days[time->day - 1], 
        time->date, 
        time->month, 
        time->year, 
        time->hours,
        time->minutes,
        time->seconds
    );
}


void rtc__print_time(rtc_time_t *time) {

    printf("%.2d:%.2d:%.2d\n", time->hours, time->minutes, time->seconds);
}
