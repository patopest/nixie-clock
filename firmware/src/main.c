#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
#include "pico/binary_info.h"
#include "pico/multicore.h"
#include "hardware/gpio.h"
#include "hardware/rtc.h"

#include "config.h"
#include "segment_display.h"
#include "nixie_display.h"
#include "rtc.h"
#include "shell.h"
#include "eeprom.h"
#include "button.h"
#include "timecode_decoder.h"
#include "timecode_encoder.h"
#include "usb.h"
#include "midi.h"


/* ---------- Local Defines ---------- */
enum mode_e {
    MODE_CLOCK = 0,
    MODE_TC_GENERATOR,
    MODE_TC_MONITOR,

    MODE_MAX,
};

enum display_mode_e {
    DISPLAY_HOURS = 0,
    DISPLAY_MINUTES,
    DISPLAY_SECONDS,
};

enum timcode_monitor_mode_e {
    TC_MONITOR_LTC = 0,
    TC_MONITOR_MTC,
};


/* ---------- Global Variables ---------- */
extern bool UART_RX_DONE_FLAG;
uint8_t DISPLAY_MODE = DISPLAY_HOURS;

/* ---------- Local Variables ---------- */
static bool running = true;
static uint8_t mode = MODE_CLOCK;
static uint8_t tc_monitor_mode = TC_MONITOR_LTC;
static rtc_time_t time;

/* ---------- Local Functions ---------- */
void main_core1();
void mode_button_callback(uint8_t button, button_event_t event);
void play_button_callback(uint8_t button, button_event_t event);
void reset_button_callback(uint8_t button, button_event_t event);



void mode_button_callback(uint8_t button, button_event_t event) {

    if (event == BUTTON_EVENT_ON_PRESS) {
        if (++mode == MODE_MAX) mode = 0;

        if (mode == MODE_CLOCK) {
            printf("Setting mode to CLOCK\n");
            timecode_decoder__stop();
            rtc__set_mode(RTC_MODE_I2C);
            running = true;
        }
        else if (mode == MODE_TC_GENERATOR) {
            printf("Setting mode to TC_GENERATOR\n");
            time.seconds = 0;
            time.minutes = 0;
            time.hours = 0;
            time.frame = 0;
            // rtc__set_mode(RTC_MODE_PICO);
            // rtc__write_time(&time, RTC_DATETIME_SIZE);
            // timecode_encoder__reset_time();
            // timecode_encoder__set_fps(30);
            timecode_encoder__start();
        }
        else {
            printf("Setting mode to TC_MONITOR\n");
            timecode_encoder__stop();
            timecode_decoder__reset_time();
            midi__reset_time();
            timecode_decoder__start();
            running = true;
        }
    }
}


void play_button_callback(uint8_t button, button_event_t event) {

    if (event == BUTTON_EVENT_ON_PRESS && mode == MODE_TC_GENERATOR) {
        printf("Play button pressed: %d\n", !running);
        if (running == true) {
            running = false;
        }
        else {
            rtc__write_time(&time, RTC_DATETIME_SIZE);
            running = true;
        }
    }
}


void reset_button_callback(uint8_t button, button_event_t event) {

    if (event == BUTTON_EVENT_ON_PRESS) {
        if (mode == MODE_TC_GENERATOR) {
            printf("Reset button pressed\n");
            time.seconds = 0;
            time.minutes = 0;
            time.hours = 0;
            time.frame = 0;
            rtc__write_time(&time, RTC_DATETIME_SIZE);
        }
        else if (mode == MODE_TC_MONITOR) {
            if (tc_monitor_mode == TC_MONITOR_LTC) {
                printf("Setting monitor mode to MTC\n");
                tc_monitor_mode = TC_MONITOR_MTC;
            }
            else {
                printf("Setting monitor mode to LTC\n");
                tc_monitor_mode = TC_MONITOR_LTC;
            }
        }
    }
}



int main() {

    bi_decl(bi_program_description("A simple clock"));

    stdio_init_all();

    printf("\n\n\nHello from core 0\n");

    gpio_init(LED_PIN);
    gpio_set_dir(LED_PIN, GPIO_OUT);
    bi_decl(bi_1pin_with_name(LED_PIN, "On-board LED"));
    // // debug pin
    gpio_init(DEBUG_PIN);
    bi_decl(bi_1pin_with_name(DEBUG_PIN, "Debug pin"));
    gpio_set_dir(DEBUG_PIN, GPIO_OUT);

    segment_display__init();

    rtc__init();
    eeprom__init();
    nixie_display__init();

    button__init();
    button__set_callback(MODE_BUTTON, &mode_button_callback);
    button__set_callback(PLAY_BUTTON, &play_button_callback);
    button__set_callback(RESET_BUTTON, &reset_button_callback);
    usb__init();
    midi__init();
    shell__init();

    // rtc__read_time(&time, RTC_DATETIME_SIZE);
    printf("Time: ");
    // rtc__print_datetime(&time);

    multicore_launch_core1(main_core1);

    while (1) {
        uint8_t digit1, digit2, digit3, digit4, digit5, digit6;

        gpio_put(LED_PIN, 1);

        if (UART_RX_DONE_FLAG) {
            UART_RX_DONE_FLAG = false;
            shell__process_command();
        }

        button__check_states();

        // setting clock
        if (running == true) {
            if (mode == MODE_CLOCK) {
                rtc__read_time(&time, RTC_TIME_SIZE);
            }
            else if (mode == MODE_TC_GENERATOR) {
                timecode_encoder__read_time(&time);
            }
            else { // MODE_TC_MONITOR
                if (tc_monitor_mode == TC_MONITOR_LTC) {
                    timecode_decoder__read_time(&time);
                }
                else {
                    midi__read_time(&time);
                }
            }
        }

        if (DISPLAY_MODE == DISPLAY_SECONDS) {
            digit1 = time.seconds / 10; // seconds 2nd digit
            digit2 = time.seconds % 10; // seconds 1rst digit
            digit3 = time.frame / 10;   // frames 2nd digit
            digit4 = time.frame % 10;   // frames 1rst digit
            digit5 = DIGIT_OFF;
            digit6 = DIGIT_OFF;
        }
        else if (DISPLAY_MODE == DISPLAY_MINUTES) {
            digit1 = time.minutes / 10; // minutes 2nd digit
            digit2 = time.minutes % 10; // minutes 1rst digit
            digit3 = time.seconds / 10; // seconds 2nd digit
            digit4 = time.seconds % 10; // seconds 1rst digit
            digit5 = time.frame / 10;   // frames 2nd digit
            digit6 = time.frame % 10;   // frames 1rst digit
        }
        else {
            digit1 = time.hours / 10;   // hours 2nd digit
            digit2 = time.hours % 10;   // hours 1rst digit
            digit3 = time.minutes / 10; // minutes 2nd digit
            digit4 = time.minutes % 10; // minutes 1rst digit
            digit5 = time.seconds / 10; // seconds 2nd digit
            digit6 = time.seconds % 10; // seconds 1rst digit
        }
        // printf("time: %d %d : %d %d\n", digit1, digit2, digit3, digit4);
        segment_display__set_digit(0, digit1);
        segment_display__set_digit(1, digit2);
        segment_display__set_digit(2, digit3);
        segment_display__set_digit(3, digit4);

        nixie_display__set_digit(0, digit1);
        nixie_display__set_digit(1, digit2);
        nixie_display__set_digit(2, digit3);
        nixie_display__set_digit(3, digit4);
        nixie_display__set_digit(4, digit5);
        nixie_display__set_digit(5, digit6);

        nixie_display__write_digits();

        gpio_put(LED_PIN, 0);
        sleep_ms(10);

    }
}


void main_core1() {
    rtc_time_t current_timecode, new_timecode;

    printf("Hello from core 1\n");
    sleep_ms(1000);

    if (TIMECODE_ENABLE == true) {
        timecode_decoder__init();
        timecode_encoder__init();
    }

    while(1) {
        // timecode_decoder__run();
        // timecode_decoder__read_time(&new_timecode);
        timecode_encoder__run();
        timecode_encoder__read_time(&new_timecode);
        // if (memcmp(&current_timecode, &new_timecode, sizeof(rtc_time_t)) != 0) {
        //     current_timecode = new_timecode;
        //     midi__send_timecode(&current_timecode);
        // }
        // usb__run();
    }
}