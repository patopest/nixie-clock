#ifndef USB_H
#define USB_H

/* ---------- Global Functions ---------- */
void usb__init();
void usb__run();

#endif