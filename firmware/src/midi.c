#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"

#include "midi.h"
#include "rtc.h"


/* ---------- Local Variables ---------- */
#define BUFFER_SIZE  3


/* ---------- Local Variables ---------- */
static rtc_time_t time;
static midi_packet_t buffer[BUFFER_SIZE];
static uint8_t buffer_index = 0;



void midi__init() {

    printf("Init MIDI module\n");
}


void midi__handle_packet(midi_packet_t packet) {

    if (packet.data.np.status == MIDI_STATUS_NOTE_ON) {
        printf("Note ON - note: %d, velocity: %d\n", packet.data.np.note, packet.data.np.velocity);
    }
    else if (packet.data.np.status == MIDI_STATUS_NOTE_OFF) {
        printf("Note OFF - note: %d, velocity: %d\n", packet.data.np.note, packet.data.np.velocity);
    }
    else if (packet.data.extc.status == MIDI_STATUS_SYSEX) {
        if ((packet.data.extc.sysex_id == MIDI_SYSEX_ID_REAL_TIME) && (packet.data.extc.sub_id1 == MIDI_SYSEX_SUB1_TIMECODE) &&
            (packet.data.extc.sub_id2 == MIDI_SYSEX_SUB2_FULL_TIMECODE) && (packet.data.extc.end == MIDI_STATUS_SYSEX_EOX)) {

            printf("Sysex timecode - %02d:%02d:%02d:%02d\n", packet.data.extc.hours, packet.data.extc.minutes, packet.data.extc.seconds, packet.data.extc.frames);
            time.hours = packet.data.extc.hours;
            time.minutes = packet.data.extc.minutes;
            time.seconds = packet.data.extc.seconds;
            time.frame = packet.data.extc.frames;
        }
    }
    else {
        printf("Unhandled MIDI status: 0x%x\n", packet.data.cp.status);
    }
}


void midi__read_time(rtc_time_t *rtc_time) { 
    rtc_time->hours = time.hours;
    rtc_time->minutes = time.minutes;
    rtc_time->seconds = time.seconds;
    rtc_time->frame = time.frame;
}


void midi__reset_time() {
    memset(&time, 0x00, sizeof(rtc_time_t));
}


uint8_t midi__available_packet() {
    return buffer_index;
}


void midi__read_packet(midi_packet_t *out) {
    if (buffer_index > 0) {
        buffer_index -= 1;
        // printf("Reading packet at index %d\n", buffer_index);
        *out = buffer[buffer_index];
        memset(&buffer[buffer_index], 0x00, sizeof(midi_packet_t));
    }
    else {
        memset(out, 0x00, sizeof(midi_packet_t));
    }
}


void midi__send_timecode(rtc_time_t *time) {
    midi_sysex_timecode_t extc = {
        .status = MIDI_STATUS_SYSEX,
        .sysex_id = MIDI_SYSEX_ID_REAL_TIME,
        .device_id = MIDI_SYSEX_DEVICE_ID_ALL,
        .sub_id1 = MIDI_SYSEX_SUB1_TIMECODE,
        .sub_id2 = MIDI_SYSEX_SUB2_FULL_TIMECODE,
        
        .hours = time->hours,
        .minutes = time->minutes,
        .seconds = time->seconds,
        .frames = time->frame,
        .end = MIDI_STATUS_SYSEX_EOX,
    };

    midi_packet_t packet = {
        .length = MIDI_SYSEX_TC_PACKET_LENGTH,
        .data = (midi_packet_u) extc,
    };

    // printf("Setting TC packet at index %d\n", buffer_index);
    buffer[buffer_index] = packet;
    buffer_index += 1;
}




