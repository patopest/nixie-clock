#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"

#include "timecode.h"

#include "rtc.h"


/* ---------- Local Defines ---------- */


/* ---------- Local Variables ---------- */


/* ---------- Local Functions ---------- */
static void handle_drop_frame(ltc_frame_t *frame);



void time_to_ltc_frame(rtc_time_t *time, ltc_frame_t *frame, uint8_t fps, uint8_t flags) {

    frame->frame_units = time->frame % 10;
    frame->frame_tens = time->frame / 10;
    frame->seconds_units = time->seconds % 10;
    frame->seconds_tens = time->seconds / 10;
    frame->minutes_units = time->minutes % 10;
    frame->minutes_tens = time->minutes / 10;
    frame->hours_units = time->hours % 10;
    frame->hours_tens = time->hours / 10;

    // Add datetime in the user bits
    if (flags & LTC_BGF_DATETIME) {
        frame->user_bits1 = time->day % 10;
        frame->user_bits2 = time->day / 10;
        frame->user_bits3 = time->month % 10;
        frame->user_bits4 = time->month / 10;
        frame->user_bits5 = time->year % 10;
        frame->user_bits6 = time->year / 10;
    }

    // Set Binary Group flags
    if (fps == LTC_25FPS) { // BGF 0 and 2 positions change at 25 fps
        frame->polarity_correction = (flags & 0b001);
        frame->binary_group_flag1  = (flags & 0b010) >> 1;
        frame->binary_group_flag0  = (flags & 0b100) >> 2;
    }
    else {
        frame->binary_group_flag0  = (flags & 0b001);
        frame->binary_group_flag1  = (flags & 0b010) >> 1;
        frame->binary_group_flag2  = (flags & 0b100) >> 2;
    }

    // Set other flags
    if (flags & LTC_COLOR_FRAME) {
        frame->color_frame = 1;
    }
    if ((flags & LTC_DROP_FRAME) || (frame->drop_frame)) {
        frame->drop_frame = 1;
        handle_drop_frame(frame);
    }

    frame->sync_word = LTC_SYNC_WORD;

    // compute parity here (need to recompute if user_bits are set later)
    compute_parity(frame, fps);
}


void frame_to_time(ltc_frame_t *frame, rtc_time_t *time) {

    time->frame   = frame->frame_units + (frame->frame_tens * 10);
    time->seconds = frame->seconds_units + (frame->seconds_tens * 10);
    time->minutes = frame->minutes_units + (frame->minutes_tens * 10);
    time->hours   = frame->hours_units + (frame->hours_tens * 10);

    uint8_t bgf = (frame->binary_group_flag2 << 2) | frame->binary_group_flag0;
    if (bgf & LTC_BGF_DATETIME) {
        time->date  = frame->user_bits1 + (frame->user_bits2 * 10);
        time->month = frame->user_bits3 + (frame->user_bits4 * 10);
        time->year  = frame->user_bits5 + (frame->user_bits6 * 10);
    }

}


void frame_increment(ltc_frame_t *frame, uint8_t fps) {

    uint8_t frames  = frame->frame_units + (frame->frame_tens * 10);
    uint8_t seconds = frame->seconds_units + (frame->seconds_tens * 10);
    uint8_t minutes = frame->minutes_units + (frame->minutes_tens * 10);
    uint8_t hours   = frame->hours_units + (frame->hours_tens * 10);

    frames++;

    if (frames >= fps) {
        frames = 0;
        seconds++;
        if (seconds == 60) {
            seconds = 0;
            minutes++;
            if (minutes == 60) {
                minutes = 0;
                hours++;
                if (hours == 24) {
                    hours = 0;
                    // TODO: handle date
                }
            }
        }
    }

    frame->frame_units = frames % 10;
    frame->frame_tens = frames / 10;
    frame->seconds_units = seconds % 10;
    frame->seconds_tens = seconds / 10;
    frame->minutes_units = minutes % 10;
    frame->minutes_tens = minutes / 10;
    frame->hours_units = hours % 10;
    frame->hours_tens = hours / 10;

    if (frame->drop_frame) {
        handle_drop_frame(frame);
    }

    compute_parity(frame, fps);
}


bool compute_parity(ltc_frame_t *frame, uint8_t fps) {
    
    uint8_t parity = 0; // each bit stores the parity for a byte.

    // Reset parity bit (if already set)
    if (fps == LTC_25FPS) {
        frame->binary_group_flag2 = 0;
    }
    else {
        frame->polarity_correction = 0;
    }

    // calculate parity for each byte
    for (uint8_t i = 0; i < LTC_FRAME_SIZE; i++) {
        parity ^= frame->bytes[i];
    }

    // calculate final parity bit
    parity ^= parity >> 8;
    parity ^= parity >> 4;
    parity ^= parity >> 2;
    parity ^= parity >> 1;

    // set parity bit
    if (fps == LTC_25FPS) {
        frame->binary_group_flag2 = parity & 1;
    }
    else {
        frame->polarity_correction = parity & 1;
    }

    return parity & 1;


}


// Skip the first two frames of very minutes unless minutes is divisble by ten (aka 0, 10, 20, 30, 40, 50)
// https://en.wikipedia.org/wiki/SMPTE_timecode#Drop-frame_timecode
static void handle_drop_frame(ltc_frame_t *frame) {
    // Check minute is not divisible by ten
    if (frame->minutes_units != 0) {
        // Check it's the start of the minute (frame = 0 and seconds = 0)
        uint8_t frames = frame->frame_units + (frame->frame_tens * 10);
        uint8_t seconds = frame->seconds_units + (frame->seconds_tens * 10);
        if (frame == 0 && seconds == 0) {
            frame->frame_units += 2;
        }
    }
}


void print_ltc_frame(ltc_frame_t *frame) {

    uint8_t frames = frame->frame_units + (frame->frame_tens * 10);
    uint8_t seconds = frame->seconds_units + (frame->seconds_tens * 10);
    uint8_t minutes = frame->minutes_units + (frame->minutes_tens * 10);
    uint8_t hours = frame->hours_units + (frame->hours_tens * 10);

    printf("%.2d:%.2d:%.2d:%.2d\n", 
        hours,
        minutes,
        seconds,
        frames
    );
}



