#include <stdio.h>
#include "pico/stdlib.h"
#include "pico/binary_info.h"
#include "hardware/i2c.h"

#include "nixie_display.h"
#include "config.h"


/*
Each IO expander chip feeds two nixie drivers with BCD decimals
Each nixie tube digit is encoded on 4 bits with the 4 MSB for tube #1 and 4 LSB for tube #2
    bits: 8 7 6 5 | 4 3 2 1
    tube:    1         2
*/


/* ---------- Local Defines ---------- */
// I2C PCF8574A module
#define PCF8574A_ADDR           0x38


/* ---------- Local Variables ---------- */
// array containing the current digits for each display (value of 0xFF means no display)
static uint8_t current_digits[NIXIE_NUM_DISPLAYS] = {DIGIT_OFF};
static bool sequence_enable = false;
static struct repeating_timer trigger_sequence_timer;
static struct repeating_timer sequence_timer;


/* ---------- Local Functions ---------- */
static void nixie_display__write(uint8_t chip_id, uint8_t digits[2]);
static bool nixie_display__cycle_digits_sequence();


void nixie_display__init() {

    if (NIXIE_ENABLE == false) {
        return;
    }
    printf("Init Nixie Display module\n");

    bi_decl(bi_2pins_with_names(NIXIE_I2C_SDA_PIN, "PCF8574A Nixie", NIXIE_I2C_SCL_PIN, "PCF8574A Nixie"));

    // add timer to trigger sequence
    add_repeating_timer_ms(NIXIE_SEQUENCE_INTERVAL * 60000, nixie_display__trigger_sequence, NULL, &trigger_sequence_timer);
}


void nixie_display__set_digit(uint8_t display, uint8_t digit) {

    if (display >= NIXIE_NUM_DISPLAYS) {
        return;
    }
    if (sequence_enable == false) {
        current_digits[display] = digit;
    }
}


void nixie_display__write_digits() {

    if (NIXIE_ENABLE == false) {
        return;
    }

    // printf("Writing Nixie digits: ");
    for (uint8_t chip_id = 0; chip_id < NIXIE_NUM_CHIPS; chip_id++) {
        nixie_display__write(chip_id, &current_digits[chip_id * 2]);
    }
    // printf("\n");
}


static void nixie_display__write(uint8_t chip_id, uint8_t digits[2]) {
 
    uint8_t buffer = (digits[0] << 4) | (digits[1] & 0xFF);
    // printf("0x%x ", buffer);
    i2c_write_blocking(NIXIE_I2C, PCF8574A_ADDR + chip_id, &buffer, 1, false);
}


bool nixie_display__trigger_sequence() {
    sequence_enable = true;

    printf("Triggering Nixie sequence\n");
    add_repeating_timer_ms(-NIXIE_SEQUENCE_STEPS, nixie_display__cycle_digits_sequence, NULL, &sequence_timer);

    return true;
}


// Cycle through all digits to avoid cathode poisoning
static bool nixie_display__cycle_digits_sequence() {
    static uint8_t digit = 0;

    if (digit == 10) {
        digit = 0;
        sequence_enable = false;
        return false;
    }

    printf("Nixie sequence: Setting digits at %d\n", digit);

    for (uint8_t i = 0; i < NIXIE_NUM_DISPLAYS; i++) {
        // nixie_display__set_digit(i, digit);
        current_digits[i] = digit;
    }

    digit += 1;

    return true;
}