#ifndef SEGMENT_DISPLAY_H
#define SEGMENT_DISPLAY_H


/* ---------- Global Functions ---------- */
void segment_display__init();
void segment_display__set_digit(uint8_t display, int8_t digit);


#endif