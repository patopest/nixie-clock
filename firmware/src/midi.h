#ifndef MIDI_H
#define MIDI_H

#include "rtc.h"

/* ---------- Global Definitions ---------- */
#define PACKED __attribute__((packed))

// MIDI 1.0 Status byte, always has 1 as MSB
enum midi_status_e {
    // Channel Messages, only look at first 4 MSB
    MIDI_STATUS_NOTE_OFF            = 0x8,
    MIDI_STATUS_NOTE_ON             = 0x9,
    MIDI_STATUS_POLY_KEY_PRESSURE   = 0xA,
    MIDI_STATUS_CONTROL_CHANGE      = 0xB,
    MIDI_STATUS_PROGRAM_CHANGE      = 0xC,
    MIDI_STATUS_CHANNEL_PRESSURE    = 0xD,
    MIDI_STATUS_PITCH_BEND          = 0xE,

    // System Messages, look at whole byte
    MIDI_STATUS_SYSEX               = 0xF0,
    MIDI_STATUS_SYSEX_EOX           = 0xF7,
};

// MIDI System Exclusive IDs (byte 2)
enum midi_sysex_id_e {
    MIDI_SYSEX_ID_NON_REAL_TIME     = 0x7E,
    MIDI_SYSEX_ID_REAL_TIME         = 0x7F,
};

// MIDI System Exclusive Device IDs (byte 3)
enum midi_sysex_device_id_e {
    MIDI_SYSEX_DEVICE_ID_ALL        = 0x7F,
};

// MIDI System Exclusive Sub IDs 1 (byte 4)
enum midi_sysex_sub_id1_e {
    MIDI_SYSEX_SUB1_TIMECODE        = 0x01,
};

// MIDI System Exclusive Sub IDs 2 (byte 5)
enum midi_sysex_sub_id2_e {
    MIDI_SYSEX_SUB2_FULL_TIMECODE   = 0x01,
    MIDI_SYSEX_SUB2_USER_BITS       = 0x02,
};


// Little-Endian => bit fields are inversed

// The Channel packet (Mode and Voice)
struct midi_channel_packet_s {
    uint8_t n : 4;      // LSB
    uint8_t status : 4; // MSB

    uint8_t byte1;
    uint8_t byte2;
} PACKED;
typedef struct midi_channel_packet_s midi_channel_packet_t;

// Specific Channel packet for Note On and Note Off
struct midi_note_packet_s {
    uint8_t channel : 4;
    uint8_t status : 4;

    uint8_t note;
    uint8_t velocity;
} PACKED;
typedef struct midi_note_packet_s midi_note_packet_t;

// System Exclusive packets
struct midi_sysex_timecode_s {
    uint8_t status;
    uint8_t sysex_id;
    uint8_t device_id;
    uint8_t sub_id1;
    uint8_t sub_id2;

    uint8_t hours : 5;  // LSB
    uint8_t type : 2;
    uint8_t zero : 1;   // MSB

    uint8_t minutes;
    uint8_t seconds;
    uint8_t frames;
    uint8_t end;
} PACKED;
typedef struct midi_sysex_timecode_s midi_sysex_timecode_t;

// System Common packets
struct midi_syscom_timecode_s {
    uint8_t status;

    uint8_t zero : 1;   // LSB
    uint8_t type : 3;   
    uint8_t data : 4;   // MSB
} PACKED;
typedef struct midi_syscom_timecode_s midi_syscom_timecode_t;


enum midi_packet_length_e {
    MIDI_CHANNEL_PACKET_LENGTH   = sizeof(midi_channel_packet_t),
    MIDI_NOTE_PACKET_LENGTH      = sizeof(midi_note_packet_t),
    MIDI_SYSEX_TC_PACKET_LENGTH  = sizeof(midi_sysex_timecode_t),
    MIDI_SYSCOM_TC_PACKET_LENGTH = sizeof(midi_syscom_timecode_t),
};

typedef union {
    midi_channel_packet_t cp;
    midi_note_packet_t np;
    midi_sysex_timecode_t extc;
    midi_syscom_timecode_t comtc;
} midi_packet_u;

// The overall MIDI packet
struct midi_packet_s {
    uint8_t length;
    midi_packet_u data;
};
typedef struct midi_packet_s midi_packet_t;


/* ---------- Global Functions ---------- */
void midi__init();
void midi__handle_packet(midi_packet_t packet);
void midi__read_time(rtc_time_t *rtc_time);
void midi__reset_time();

uint8_t midi__available_packet();
void midi__read_packet(midi_packet_t *out);
void midi__send_timecode(rtc_time_t *time);




#endif