#ifndef TIMECODE_ENCODER_H
#define TIMECODE_ENCODER_H

#include "rtc.h"

/* ---------- Global Functions ---------- */
void timecode_encoder__init();
void timecode_encoder__start();
void timecode_encoder__stop();
void timecode_encoder__run();
void timecode_encoder__set_fps(uint8_t fps);
void timecode_encoder__set_flags(uint8_t flags);
void timecode_encoder__set_time(rtc_time_t *time);
void timecode_encoder__read_time(rtc_time_t *time);
void timecode_encoder__reset_time();

#endif