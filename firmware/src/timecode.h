#ifndef TIMECODE_H
#define TIMECODE_H

#include "rtc.h"

/* ---------- Global Definitions ---------- */
#define LTC_FRAME_SIZE          10 // in bytes
#define LTC_FRAME_SIZE_BITS     80 // in bits
#define LTC_SYNC_WORD           (0b1011111111111100)


enum ltc_framerates_e {
    LTC_24FPS    = 24,
    LTC_25FPS    = 25,
    LTC_29_97FPS = 30, // use drop-frame bit
    LTC_30FPS    = 30,
};


// Bitmask based on https://www.itu.int/rec/R-REC-BR.780-2-200504-W/en (technically superseded but unable to find newer version)
// BGF1: 1 = time is referenced from external clock, 0 = freerunning
// BGF0 and 2: define the character encoding of the user bits.
enum ltc_binary_group_flags_e {
    // BGF1
    LTC_BGF_CLOCK_EXTERNAL  = 0b010,
    LTC_BGF_CLOCK_FREE      = 0b000,

    // BGF2 and BGF0
    LTC_BGF_UNSPECIFIED     = 0b000,
    LTC_BGF_8BIT_CODES      = 0b001,
    LTC_BGF_DATETIME        = 0b100,
    LTC_BGF_PAGELINE        = 0b101,
};

// Additional flags
enum ltc_flags_e {
    LTC_COLOR_FRAME = 0b01000,
    LTC_DROP_FRAME  = 0b10000,

    LTC_FLAGS_NONE  = 0b00000,
};

// https://en.wikipedia.org/wiki/Linear_timecode#Longitudinal_timecode_data_format
struct __packed_aligned ltc_frame_s {
    union {
        struct {
            // Byte 0
            uint frame_units:4;
            uint user_bits1:4;

            // Byte 1
            uint frame_tens:2;
            uint drop_frame:1;
            uint color_frame:1;
            uint user_bits2:4;
            
            // Byte 2
            uint seconds_units:4;
            uint user_bits3:4;

            // Byte 3
            uint seconds_tens:3;
            uint polarity_correction:1; // or binary_group_flag0 at 25fps
            uint user_bits4:4;

            // Byte 4
            uint minutes_units:4;
            uint user_bits5:4;

            // Byte 5
            uint minutes_tens:3;
            uint binary_group_flag0:1; // or binary_group_flag2 at 25 fps
            uint user_bits6:4;

            // Byte 6
            uint hours_units:4;
            uint user_bits7:4;

            // Byte 7
            uint hours_tens:2;
            uint binary_group_flag1:1;
            uint binary_group_flag2:1; // or polarity_correction at 25fps
            uint user_bits8:4;
            
            // Byte 8 & 9
            uint16_t sync_word; 
        };
        uint8_t bytes[LTC_FRAME_SIZE];
    };
};
typedef struct ltc_frame_s ltc_frame_t;


/* ---------- Global Functions ---------- */
void time_to_ltc_frame(rtc_time_t *time, ltc_frame_t *frame, uint8_t fps, uint8_t flags);
void frame_to_time(ltc_frame_t *frame, rtc_time_t *time);
void frame_increment(ltc_frame_t *frame, uint8_t fps);
bool compute_parity(ltc_frame_t *frame, uint8_t fps);
void print_ltc_frame(ltc_frame_t *frame);

#endif