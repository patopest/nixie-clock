#ifndef RTC_H
#define RTC_H

/* ---------- Global Definitions ---------- */
#define RTC_TIME_SIZE        3
#define RTC_DATETIME_SIZE    7
#define RTC_TIMECODE_SIZE    8

struct __packed_aligned time_s {
    union {
        struct {
            // RTC data
            uint8_t seconds;
            uint8_t minutes;
            uint8_t hours;
            uint8_t day;
            uint8_t date;
            uint8_t month;
            uint8_t year;

            // Additional data for SMPTE timecode
            uint8_t frame;
        };
        uint8_t array[8];
    };
};
typedef struct time_s rtc_time_t;

enum rtc_mode_e {
    RTC_MODE_I2C = 0, // Use the I2C RTC module
    RTC_MODE_PICO,    // Use the pico's hardware RTC module
};


/* ---------- Global Variable ---------- */
extern char *days[7];


/* ---------- Global Functions ---------- */
void rtc__init();
void rtc__get_config();
void rtc__set_mode(uint8_t rtc_mode);

void rtc__read_time(rtc_time_t *time, uint8_t length);
void rtc__write_time(rtc_time_t *time, uint8_t length);
void rtc__set_calibration(int8_t value);

void rtc__print_datetime(rtc_time_t *time);
void rtc__print_time(rtc_time_t *time);


#endif