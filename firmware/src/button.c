#include <stdio.h>
#include <string.h>
#include "pico/binary_info.h"
#include "hardware/gpio.h"

#include "button.h"
#include "config.h"


/* ---------- Local Variables ---------- */
static uint8_t buttons[NUM_BUTTONS] = {BUTTON_0_PIN, BUTTON_1_PIN, BUTTON_2_PIN};
static button_event_t button_states[NUM_BUTTONS] = {BUTTON_EVENT_NONE};
static button_callback_t callbacks[NUM_BUTTONS] = {NULL};


/* ---------- Local Functions ---------- */
static void gpio_isr_callback(uint gpio, uint32_t events);



void button__init() {

    printf("Init GPIO buttons\n");

    gpio_init(BUTTON_0_PIN);
    gpio_set_irq_enabled_with_callback(BUTTON_0_PIN, GPIO_IRQ_EDGE_RISE | GPIO_IRQ_EDGE_FALL, true, &gpio_isr_callback);
    bi_decl(bi_1pin_with_name(BUTTON_0_PIN, "Button 1"));

    gpio_init(BUTTON_1_PIN);
    gpio_set_irq_enabled_with_callback(BUTTON_1_PIN, GPIO_IRQ_EDGE_RISE | GPIO_IRQ_EDGE_FALL, true, &gpio_isr_callback);
    bi_decl(bi_1pin_with_name(BUTTON_1_PIN, "Button 2"));

    gpio_init(BUTTON_2_PIN);
    gpio_set_irq_enabled_with_callback(BUTTON_2_PIN, GPIO_IRQ_EDGE_RISE | GPIO_IRQ_EDGE_FALL, true, &gpio_isr_callback);
    bi_decl(bi_1pin_with_name(BUTTON_2_PIN, "Button 3"));
}


void button__set_callback(uint8_t button, button_callback_t callback) {
    if (button < NUM_BUTTONS) {
        callbacks[button] = callback;
    }
}


static void gpio_isr_callback(uint gpio, uint32_t events) {
    // printf("GPIO %d %d\n", gpio, events);
    button_event_t event = BUTTON_EVENT_NONE;

    if (events & 0x08) {
        event = BUTTON_EVENT_ON_PRESS;
    }
    if (events & 0x04) {
        event = BUTTON_EVENT_ON_RELEASE;
    }

    for (uint8_t i = 0; i < NUM_BUTTONS; i++) {
        if (buttons[i] == gpio) {
            button_states[i] = event;
        }
    }
}


void button__check_states() {

    for (uint8_t i = 0; i < NUM_BUTTONS; i++) {
        if (button_states[i] != BUTTON_EVENT_NONE && callbacks[i] != NULL) {
            button_callback_t callback = callbacks[i];
            callback(i, button_states[i]);
            button_states[i] = BUTTON_EVENT_NONE;
        }
    }
}


void button__trigger_callback(uint8_t button) {
    if (callbacks[button] != NULL) {
        button_callback_t callback = callbacks[button];
        callback(button, BUTTON_EVENT_ON_PRESS);
    }
}
