#ifndef BUTTON_H
#define BUTTON_H

/* ---------- Global Definitions ---------- */
typedef enum button_event_e {
    BUTTON_EVENT_NONE = 0,
    BUTTON_EVENT_ON_PRESS,
    BUTTON_EVENT_ON_RELEASE,
} button_event_t;

typedef void(* button_callback_t) (uint8_t button, button_event_t event);



/* ---------- Global Functions ---------- */
void button__init();
void button__set_callback(uint8_t button, button_callback_t callback);
void button__trigger_callback(uint8_t button);
void button__check_states();

#endif