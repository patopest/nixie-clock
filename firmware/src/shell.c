#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "pico/stdlib.h"
#include "hardware/uart.h"
#include "hardware/irq.h"
#include "hardware/watchdog.h"

#include "shell.h"
#include "rtc.h"
#include "eeprom.h"
#include "button.h"
#include "nixie_display.h"


/* ---------- Local Defines ---------- */
#define DATA_BITS 8
#define STOP_BITS 1
#define PARITY    UART_PARITY_NONE

#define BUFFER_LENGTH       (100u)


/* ---------- Global Variables ---------- */
bool UART_RX_DONE_FLAG = false;
extern uint8_t DISPLAY_MODE;


/* ---------- Local Variables ---------- */
static bool echo = true;                // echo back received chars
static char buffer[BUFFER_LENGTH];      // static circular buffer
static char *buffer_write;



void shell__init() {
    // Turn off FIFO's - we want to do this character by character
    // uart_set_fifo_enabled(uart_default, false);

    // Setup buffer
    buffer_write = buffer;

    // // Set UART flow control CTS/RTS, we don't want these, so turn them off
    // uart_set_hw_flow(uart_default, false, false);

    // // Set our data format
    // uart_set_format(uart_default, DATA_BITS, STOP_BITS, PARITY);

    // Set up a RX interrupt
    // We need to set up the handler first
    // Select correct interrupt for the UART we are using
    int UART_IRQ = uart_default == uart0 ? UART0_IRQ : UART1_IRQ;

    // And set up and enable the interrupt handlers
    irq_set_exclusive_handler(UART_IRQ, shell__on_uart_rx);
    irq_set_enabled(UART_IRQ, true);

    // Now enable the UART to send interrupts - RX only
    uart_set_irq_enables(uart_default, true, false);
}


// RX interrupt handler
void shell__on_uart_rx() {
    while (uart_is_readable(uart_default)) {
        char ch = uart_getc(uart_default);
        
        if (ch == '\r' || ch == '\n') {
            // printf("end of command\n");
            *buffer_write = '\0';
            buffer_write++;
            UART_RX_DONE_FLAG = true;
        }
        else if (ch == 8) { // backspace
            if (buffer_write > buffer) {
                buffer_write--;
            }
        }
        else if (ch != 255) {
            // printf("rec: %d\n",(uint8_t)ch);
            *buffer_write = ch;
            // dumb way to do but assumming the buffer will be big enough
            if (buffer_write <= buffer + BUFFER_LENGTH) {
                buffer_write++;
            }
        }

        // echo back
        if ((echo == true) && (ch != 255) && uart_is_writable(uart_default)) {
            uart_putc(uart_default, ch);
        }

    }
}


static void print_buffer(char *buffer) {
    // debug
    printf("\n\nbuffer: \n");
    for (char i = 0; i < BUFFER_LENGTH; i++) {
        if (&buffer[i] == buffer_write) {
            printf("%d: %c (%d) W\n",i, buffer[i], (uint8_t)buffer[i]); 
        } else {
            printf("%d: %c (%d)\n",i, buffer[i], (uint8_t)buffer[i]);
        }
    }
    printf("\n");

    printf("addresses: buffer: 0x%p write: 0x%p\n", buffer, buffer_write);
}


void shell__process_command() {

    char cmd[BUFFER_LENGTH];
    char *token;

    memset(cmd, 0x00, BUFFER_LENGTH);
    strcpy(cmd, buffer);
    buffer_write = buffer;

    // printf("cmd: %s\n", cmd);

    token = strtok(cmd, " ");

    if (strcmp(token, "echo-off") == 0) {
        printf("echo off!\n");
        echo = false;
    }
    else if (strcmp(token, "echo-on") == 0) {
        printf("echo on!\n");
        echo = true;
    }
    else if (strcmp(token, "reset") == 0) {
        watchdog_reboot(0, 0, 100);
    }
    else if (strcmp(token, "get-config") == 0) {
        rtc__get_config();
    }
    else if (strcmp(token, "set-time") == 0) {
        rtc_time_t time;

        time.hours = atoi((char *) strtok(NULL, ":"));
        time.minutes = atoi((char *) strtok(NULL, ":"));
        time.seconds = atoi((char *) strtok(NULL, ":"));

        printf("set-time ");
        rtc__print_time(&time);

        if (time.hours > 24 || time.minutes > 59 || time.seconds > 59) {
            printf("ERROR: wrong time!\n");
        }
        else {
            rtc__write_time(&time, 3);
        }

    }
    else if (strcmp(token, "set-datetime") == 0) {
        rtc_time_t time;
        char *day;

        day = strtok(NULL, " ");
        time.day = 1; // default to Monday
        for (uint8_t i = 0; i < 7; i++) {
            if (strcmp(day, days[i]) == 0) {
                time.day = i+1;
            }
        }
        time.date = atoi((char *) strtok(NULL, "-"));
        time.month = atoi((char *) strtok(NULL, "-"));
        time.year = atoi((char *) strtok(NULL, " "));

        time.hours = atoi((char *) strtok(NULL, ":"));
        time.minutes = atoi((char *) strtok(NULL, ":"));
        time.seconds = atoi((char *) strtok(NULL, ":"));

        printf("set-datetime ");
        rtc__print_datetime(&time);
        
        if (time.hours > 24 || time.minutes > 59 || time.seconds > 59) {
            printf("ERROR: wrong time!\n");
        }
        else if (time.date < 1 || time.date > 31 || time.month < 1|| time.month > 12 || time.year > 99) {
            printf("ERROR: wrong date!\n");
        }
        else {
            rtc__write_time(&time, 7);
        }

    }
    else if (strcmp(token, "get-time") == 0) {
        rtc_time_t time;

        rtc__read_time(&time, 3);
        rtc__print_time(&time);
    }
    else if (strcmp(token, "get-datetime") == 0) {
        rtc_time_t time;

        rtc__read_time(&time, 7);
        rtc__print_datetime(&time);
    }
    else if (strcmp(token, "set-calibration") == 0) {
        int8_t value = atoi((char *) strtok(NULL, " "));

        rtc__set_calibration(value);
    }
    else if (strcmp(token, "set-display") == 0) {
        uint8_t mode = atoi((char *) strtok(NULL, " "));

        printf("set-display mode %d\n", mode);

        if (mode >= 3) {
            printf("ERROR: wrong display mode!\n");
        }
        else {
            DISPLAY_MODE = mode;
        }
    }
    else if (strcmp(token, "trigger-seq") == 0) {
        printf("trigger nixie sequence\n");
        nixie_display__trigger_sequence();
    }
    else if(strcmp(token, "test") == 0) {
        eeprom__test();
    }
    else if (strcmp(token, "press-button") == 0) {
        uint8_t id = atoi((char *) strtok(NULL, " "));

        printf("pressing button %d\n", id);
        button__trigger_callback(id);
    }
    else {
        printf("unrecognised command\n");
    }
}