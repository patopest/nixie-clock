#include <stdio.h>
#include <string.h>
#include <math.h>
#include <stdlib.h>
#include "pico/stdlib.h"
#include "pico/binary_info.h" 
#include "hardware/clocks.h"
#include "hardware/gpio.h"
#include "hardware/pio.h"

#include "differential_manchester.pio.h"

#include "rtc.h"
#include "timecode.h"
#include "timecode_encoder.h"
#include "config.h"


/* ---------- Local Defines ---------- */
#define LTC_DEFAULT_FPS     LTC_30FPS


/* ---------- Local Variables ---------- */
static PIO pio;
static uint pio_sm;
// static uint pio_offset;

static rtc_time_t timecode;
static ltc_frame_t frame;
static uint8_t ltc_fps = LTC_DEFAULT_FPS;
static uint8_t ltc_flags = LTC_FLAGS_NONE;

static bool run = false;


/* ---------- Local Functions ---------- */
static float timecode_encoder__get_clkdiv(uint8_t fps);



void timecode_encoder__init() {

    if (TIMECODE_ENCODER_ENABLE == false) {
        return;
    }

    printf("Init Timecode PWM module\n");

    // GPIO setup
    gpio_set_function(TIMECODE_ENCODER_PIN, GPIO_FUNC_PIO0);
    bi_decl(bi_1pin_with_func(TIMECODE_ENCODER_PIN, GPIO_FUNC_PIO0));
    bi_decl(bi_1pin_with_name(TIMECODE_ENCODER_PIN, "LTC output"));


    timecode.year     = 23;
    timecode.month    = 10;
    timecode.day      = 21;
    timecode.hours    = 1;
    timecode.minutes  = 00;
    timecode.seconds  = 00;
    timecode.frame    = 0;

    time_to_ltc_frame(&timecode, &frame, ltc_fps, ltc_flags);


    // PIO
    pio = pio0;
    pio_sm = pio_claim_unused_sm(pio, true);
    uint pio_offset = pio_add_program(pio, &differential_manchester_tx_program);

    // Configure state machines, set bit rate (with clock divisor) based on fps
    float clock_div = timecode_encoder__get_clkdiv(ltc_fps);
    differential_manchester_tx_program_init(pio, pio_sm, pio_offset, TIMECODE_ENCODER_PIN, clock_div);
    pio_sm_set_enabled(pio, pio_sm, false);

}


static float timecode_encoder__get_clkdiv(uint8_t fps) {

    uint16_t bitrate = LTC_FRAME_SIZE_BITS * fps;
    float clock_div = clock_get_hz(clk_sys) / (16 * bitrate);
    printf("LTC Encoder: setting bitrate at %d fps = %d -> clock_div: %f\n", fps, bitrate, clock_div);

    return clock_div;
}


void timecode_encoder__start() {

    if (TIMECODE_ENCODER_ENABLE == false) {
        return;
    }
    
    printf("Starting LTC playback\n");
    pio_sm_clear_fifos(pio, pio_sm);
    pio_sm_set_enabled(pio, pio_sm, true);
    run = true;
}


void timecode_encoder__stop() {

    if (TIMECODE_ENCODER_ENABLE == false) {
        return;
    }

    printf("Stopping LTC playback\n");
    pio_sm_set_enabled(pio, pio_sm, false);
    run = false;
}


void timecode_encoder__run() {

    if (run == true) {
        gpio_put(DEBUG_PIN, 1);

        // Write 2 bytes (16 bits) of the frame as a 32-bit word in the PIO TX FIFO
        pio_sm_put_blocking(pio, pio_sm, frame.bytes[1] << 8 | frame.bytes[0]);
        pio_sm_put_blocking(pio, pio_sm, frame.bytes[3] << 8 | frame.bytes[2]);
        pio_sm_put_blocking(pio, pio_sm, frame.bytes[5] << 8 | frame.bytes[4]);
        pio_sm_put_blocking(pio, pio_sm, frame.bytes[7] << 8 | frame.bytes[6]);
        pio_sm_put_blocking(pio, pio_sm, frame.bytes[9] << 8 | frame.bytes[8]);

        print_ltc_frame(&frame);
        frame_increment(&frame, ltc_fps);

        gpio_put(DEBUG_PIN, 0);
    }
}


void timecode_encoder__set_fps(uint8_t fps) {

    float clock_div = timecode_encoder__get_clkdiv(fps);
    pio_sm_set_clkdiv(pio, pio_sm, clock_div);
    ltc_fps = fps;

}


void timecode_encoder__set_flags(uint8_t flags) {
    ltc_flags |= flags;
}


void timecode_encoder__set_time(rtc_time_t *time) {
    // memcpy(&timecode, time, sizeof(rtc_time_t));
    time_to_ltc_frame(time, &frame, ltc_fps, ltc_flags);
}


void timecode_encoder__read_time(rtc_time_t *time) {
    // memcpy(time, &timecode, sizeof(rtc_time_t));
    frame_to_time(&frame, time);
}


void timecode_encoder__reset_time() {
    memset(&timecode, 0x00, sizeof(rtc_time_t));
}






