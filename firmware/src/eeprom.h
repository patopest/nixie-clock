#ifndef EEPROM_H
#define EEPROM_H

#include "rtc.h"

/* ---------- Global Functions ---------- */
void eeprom__init();
void eeprom__read_time(rtc_time_t *time, uint8_t length);
void eeprom__write_time(rtc_time_t *time, uint8_t length);
int8_t eeprom__read_aging();
void eeprom__write_aging(uint8_t value);

void eeprom__test();


#endif