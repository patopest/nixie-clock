#include <stdio.h>
#include "pico/stdlib.h"
#include "pico/binary_info.h"
#include "hardware/gpio.h"

#include "segment_display.h"
#include "config.h"


/*
  7 Segment display has pins as follows:

  --A--
  F   B
  --G--
  E   C
  --D--

  GPIO Pin mask: G C DP D E A F B
  eg: 1 is segments B and C -> 0b01000001
*/


/* ---------- Local Defines ---------- */
#define DISPLAY_1_PIN        12
#define DISPLAY_2_PIN        11
#define DISPLAY_3_PIN        10
#define DISPLAY_4_PIN        21

#define SEGMENT_A_PIN        15
#define SEGMENT_B_PIN        13
#define SEGMENT_C_PIN        19
#define SEGMENT_D_PIN        17
#define SEGMENT_E_PIN        16
#define SEGMENT_F_PIN        14
#define SEGMENT_G_PIN        20
#define SEGMENT_POINT_PIN    18

#define MASK_OFFSET          13 // lowest segment pin to shift the mask


/* ---------- Local Variables ---------- */
// mask to perform actions on all segments
const uint8_t all_segments_mask = 0b11111111;

// lookup table of the mask for each digit
const uint8_t digit_masks[10] = {
    0b01011111, // 0
    0b01000001, // 1
    0b10011101, // 2
    0b11010101, // 3
    0b11000011, // 4
    0b11010110, // 5
    0b11011110, // 6
    0b01000101, // 7
    0b11011111, // 8
    0b11010111  // 9
};

// array containing the current displayed digit for each display (value of -1 means no display)
int8_t current_digits[SEGMENT_NUM_DISPLAYS] = {-1};

struct repeating_timer timer;


/* ---------- Local Functions ---------- */
static bool display_timer_callback(struct repeating_timer *t);
static void set_display(uint8_t display);
static void write_digit(int8_t digit);



void segment_display__init() {

    if (SEGMENT_ENABLE == false) {
        return;
    }
    printf("Init LED segment display\n");

    gpio_init(DISPLAY_1_PIN);
    gpio_set_dir(DISPLAY_1_PIN, GPIO_OUT);
    gpio_set_outover(DISPLAY_1_PIN, GPIO_OVERRIDE_INVERT);
    bi_decl(bi_1pin_with_name(DISPLAY_1_PIN, "Display 1"));

    gpio_init(DISPLAY_2_PIN);
    gpio_set_dir(DISPLAY_2_PIN, GPIO_OUT);
    gpio_set_outover(DISPLAY_2_PIN, GPIO_OVERRIDE_INVERT);
    bi_decl(bi_1pin_with_name(DISPLAY_2_PIN, "Display 2"));

    gpio_init(DISPLAY_3_PIN);
    gpio_set_dir(DISPLAY_3_PIN, GPIO_OUT);
    gpio_set_outover(DISPLAY_3_PIN, GPIO_OVERRIDE_INVERT);
    bi_decl(bi_1pin_with_name(DISPLAY_3_PIN, "Display 3"));

    gpio_init(DISPLAY_4_PIN);
    gpio_set_dir(DISPLAY_4_PIN, GPIO_OUT);
    gpio_set_outover(DISPLAY_4_PIN, GPIO_OVERRIDE_INVERT);
    bi_decl(bi_1pin_with_name(DISPLAY_4_PIN, "Display 4"));


    // Using masks for segment pins
    gpio_init_mask(all_segments_mask << MASK_OFFSET);
    gpio_set_dir_out_masked(all_segments_mask << MASK_OFFSET);
    bi_decl(bi_pin_mask_with_name(all_segments_mask << MASK_OFFSET, "Display Segments"));

    // add timer to drive displays
    add_repeating_timer_ms(-2, display_timer_callback, NULL, &timer);

}


static bool display_timer_callback(struct repeating_timer *t) {
    static uint8_t display = 0;

    if (display == 4) {
        display = 0;
    }

    set_display(display);
    write_digit(current_digits[display]);

    display++;

    return true;
}


void segment_display__set_digit(uint8_t display, int8_t digit) {

    current_digits[display] = digit;
}


static void set_display(uint8_t display) {
    switch (display) {
        case 0:
            gpio_put(DISPLAY_1_PIN, 1);
            gpio_put(DISPLAY_2_PIN, 0);
            gpio_put(DISPLAY_3_PIN, 0);
            gpio_put(DISPLAY_4_PIN, 0);
            break;
        case 1:
            gpio_put(DISPLAY_1_PIN, 0);
            gpio_put(DISPLAY_2_PIN, 1);
            gpio_put(DISPLAY_3_PIN, 0);
            gpio_put(DISPLAY_4_PIN, 0);
            break;
        case 2:
            gpio_put(DISPLAY_1_PIN, 0);
            gpio_put(DISPLAY_2_PIN, 0);
            gpio_put(DISPLAY_3_PIN, 1);
            gpio_put(DISPLAY_4_PIN, 0);
            break;
        case 3:
            gpio_put(DISPLAY_1_PIN, 0);
            gpio_put(DISPLAY_2_PIN, 0);
            gpio_put(DISPLAY_3_PIN, 0);
            gpio_put(DISPLAY_4_PIN, 1);
            break;
    }
}


static void write_digit(int8_t digit) {
    uint32_t mask = 0;

    uint32_t all_segments = all_segments_mask << MASK_OFFSET;

    if (digit >= 0) {
        mask = digit_masks[digit];
    }

    mask <<= MASK_OFFSET;

    gpio_clr_mask(all_segments);
    gpio_put_masked(all_segments, mask);
}