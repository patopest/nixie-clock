#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
#include "pico/binary_info.h"
#include "hardware/i2c.h"

#include "eeprom.h"


/* ---------- Local Defines ---------- */
// I2C AT24C32 module
#define AT24C32_ADDR            0x57

#define EEPROM_PAGE_SIZE        32u

// EEPROM map
#define EEPROM_BASE_ADDR        0x00

#define DATETIME_BASE_ADDR      (EEPROM_BASE_ADDR + 0)
#define DATETIME_SECONDS        (DATETIME_BASE_ADDR + 0)
#define DATETIME_MINUTES        (DATETIME_BASE_ADDR + 1)
#define DATETIME_HOURS          (DATETIME_BASE_ADDR + 2)
#define DATETIME_DAY            (DATETIME_BASE_ADDR + 3)
#define DATETIME_DATE           (DATETIME_BASE_ADDR + 4)
#define DATETIME_MONTH          (DATETIME_BASE_ADDR + 5)
#define DATETIME_YEAR           (DATETIME_BASE_ADDR + 6)

#define RTC_AGING               (EEPROM_BASE_ADDR + 7)


/* ---------- Local Functions ---------- */
static void eeprom__read(uint8_t *buffer, uint8_t length, uint16_t address);
static void eeprom__write(uint8_t *buffer, uint8_t length, uint16_t address);
static void eeprom__erase(uint16_t address, uint8_t length);



void eeprom__init() {

    printf("Init EEPROM module\n");

    bi_decl(bi_2pins_with_names(PICO_DEFAULT_I2C_SDA_PIN, "AT24C32 EEPROM", PICO_DEFAULT_I2C_SCL_PIN, "AT24C32 EEPROM"));
}


void eeprom__test() {

    rtc_time_t time;

    memset(&time, 0x00, sizeof(rtc_time_t));
    eeprom__read_time(&time, 7);
    rtc__print_datetime(&time);
}


static void eeprom__read(uint8_t *buffer, uint8_t length, uint16_t address) {

    uint8_t buf[2];

    buf[0] = address >> 8;
    buf[1] = address & 0xFF;
    i2c_write_blocking(i2c_default, AT24C32_ADDR, buf, 2, false);

    i2c_read_blocking(i2c_default, AT24C32_ADDR, buffer, length, false);
}


static void eeprom__write(uint8_t *buffer, uint8_t length, uint16_t address) {

    uint8_t buf[EEPROM_PAGE_SIZE + 2];

    if (length > EEPROM_PAGE_SIZE) {
        printf("Error: Page size is too big!\n");
        return;
    }

    memset(buf, 0xFF, EEPROM_PAGE_SIZE + 2);
    buf[0] = address >> 8;
    buf[1] = address & 0xFF;
    memcpy(&(buf[2]), buffer, length);

    i2c_write_blocking(i2c_default, AT24C32_ADDR, buf, length + 2, false);
}


static void eeprom__erase(uint16_t address, uint8_t length) {

    uint8_t buffer[EEPROM_PAGE_SIZE];
    memset(buffer, 0xFF, EEPROM_PAGE_SIZE);
    eeprom__write(buffer, length, address);
}


void eeprom__read_time(rtc_time_t *time, uint8_t length) {

    eeprom__read(time->array, length, DATETIME_BASE_ADDR);
}


void eeprom__write_time(rtc_time_t *time, uint8_t length) {

    eeprom__write(time->array, length, DATETIME_BASE_ADDR);
}


int8_t eeprom__read_aging() {

    uint8_t value = 0;
    eeprom__read(&value, 1, RTC_AGING);
    return (int8_t) value;
}


void eeprom__write_aging(uint8_t value) {

    eeprom__write(&value, 1, RTC_AGING);
}

