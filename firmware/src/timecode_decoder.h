#ifndef TIMECODE_DECODER_H
#define TIMECODE_DECODER_H

#include "rtc.h"

/* ---------- Global Functions ---------- */
void timecode_decoder__init();
void timecode_decoder__start();
void timecode_decoder__stop();
void timecode_decoder__run();
void timecode_decoder__read_time(rtc_time_t *time);
void timecode_decoder__reset_time();

#endif