#include <stdio.h>
#include <string.h>
#include "pico/stdlib.h"
#include "pico/binary_info.h"
#include "hardware/adc.h"
#include "hardware/dma.h"

#include "ltc.h"

#include "rtc.h"
#include "timecode_decoder.h"
#include "config.h"


/* ---------- Local Defines ---------- */
#define ADC_MIN_GPIO            26
#define ADC_MAX_GPIO            29
#define ADC_MIN_CHANNEL         0       // Channel 0 = GPIO 26, Channel 1 = GPIO 27, ...
#define ADC_MAX_CHANNEL         3

#define ADC_VREF                3.3
#define ADC_RANGE               (1 << 12)
#define ADC_CONVERT             (ADC_VREF / (ADC_RANGE - 1))
#define ADC_CLOCK_FREQ          48000000.0 // in Hz
#define ADC_SAMPLE_CYCLES       96         // number of cycles for one sample
#define ADC_SAMPLE_FREQ         48000.0    // desired sampling rate

#define DMA_BUFFER_SIZE         480


/* ---------- Local Variables ---------- */
static uint adc_channel;
static uint control_channel;
static uint8_t dma_buffer[DMA_BUFFER_SIZE];
static uint8_t *dma_buffer_p = &dma_buffer[0];
static uint8_t samples[DMA_BUFFER_SIZE];
static bool run = false;

static LTCDecoder *decoder;
static SMPTETimecode timecode;


/* ---------- Local Functions ---------- */



void timecode_decoder__init() {

    if (TIMECODE_DECODER_ENABLE == false) {
        return;
    }

    printf("Init Timecode ADC module\n");

    // ADC setup
    adc_gpio_init(TIMECODE_DECODER_PIN);
    bi_decl(bi_1pin_with_name(TIMECODE_DECODER_PIN, "LTC audio input (ADC)"));

    adc_init();
    uint8_t adc_channel_num = TIMECODE_DECODER_PIN - ADC_MIN_GPIO;
    adc_select_input(adc_channel_num);
    adc_fifo_setup(
        true,    // Write each completed conversion to the sample FIFO
        true,    // Enable DMA data request (DREQ)
        1,       // DREQ (and IRQ) asserted when at least 1 sample present
        false,   // We won't see the ERR bit because of 8 bit reads; disable.
        true     // Shift each sample to 8 bits when pushing to FIFO
    );

    float clock_div = (ADC_CLOCK_FREQ / ADC_SAMPLE_FREQ);
    adc_set_clkdiv(clock_div);

    // DMA setup
    // sleep_ms(1000);
    adc_channel = dma_claim_unused_channel(true);
    dma_channel_config cfg = dma_channel_get_default_config(adc_channel);

    // Reading from constant address, writing to incrementing byte addresses
    channel_config_set_transfer_data_size(&cfg, DMA_SIZE_8);
    channel_config_set_read_increment(&cfg, false);
    channel_config_set_write_increment(&cfg, true);

    // Pace transfers based on availability of ADC samples
    channel_config_set_dreq(&cfg, DREQ_ADC);

    dma_channel_configure(adc_channel, &cfg,
        dma_buffer,         // dst
        &adc_hw->fifo,      // src
        DMA_BUFFER_SIZE,    // transfer count
        false               // start immediately
    );

    // CONTROL channel
    control_channel = dma_claim_unused_channel(true);
    dma_channel_config cfg2 = dma_channel_get_default_config(control_channel);
    channel_config_set_transfer_data_size(&cfg2, DMA_SIZE_32);      // 32-bit txfers
    channel_config_set_read_increment(&cfg2, false);                // no read incrementing
    channel_config_set_write_increment(&cfg2, false);               // no write incrementing
    channel_config_set_chain_to(&cfg2, adc_channel);                // chain to sample chan

    dma_channel_configure(control_channel, &cfg2,
        &dma_hw->ch[adc_channel].write_addr,  // Write address (adc channel write address)
        &dma_buffer_p,                           // Read address (POINTER TO AN ADDRESS)
        1,                                    // Number of transfers, in this case each is 4 byte
        false                                 // Don't start immediately.
    );

    // LTC decoder setup
    decoder = ltc_decoder_create(1920, 32);
}


void timecode_decoder__start() {

    if (TIMECODE_DECODER_ENABLE == false) {
        return;
    }
    printf("Starting audio capture\n");
    dma_channel_start(adc_channel);
    adc_fifo_drain();
    adc_run(true);
    run = true;
}


void timecode_decoder__stop() {

    printf("Stopping audio capture\n");
    adc_run(false);
    run = false;
}


void timecode_decoder__run() {
    static long int total = 0;
    static LTCFrameExt frame;

    if (run == true) {
        dma_channel_wait_for_finish_blocking(adc_channel);
        memcpy(samples, dma_buffer, DMA_BUFFER_SIZE);
        dma_channel_start(control_channel); // Restart adc dma immediately using control dma

        ltc_decoder_write(decoder, samples, DMA_BUFFER_SIZE, total);

        while (ltc_decoder_read(decoder, &frame)) {

            ltc_frame_to_time(&timecode, &frame.ltc, 1);

            printf("%04d-%02d-%02d %s ",
                ((timecode.years < 67) ? 2000+timecode.years : 1900+timecode.years),
                timecode.months,
                timecode.days,
                timecode.timezone
                );

            printf("%02d:%02d:%02d%c%02d | %8lld %8lld%s\n",
                    timecode.hours,
                    timecode.mins,
                    timecode.secs,
                    (frame.ltc.dfbit) ? '.' : ':',
                    timecode.frame,
                    frame.off_start,
                    frame.off_end,
                    frame.reverse ? "  R" : ""
                    );
        }
        total += DMA_BUFFER_SIZE;
    }
    else {
        total = 0;
    }
}


void timecode_decoder__read_time(rtc_time_t *time) {
    time->frame = timecode.frame;
    time->seconds = timecode.secs;
    time->minutes = timecode.mins;
    time->hours = timecode.hours;
}


void timecode_decoder__reset_time() {
    memset(&timecode, 0x00, sizeof(SMPTETimecode));
}






