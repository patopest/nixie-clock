#ifndef CONFIG_H
#define CONFIG_H



/* ---------- RTC configs ---------- */
#define RTC_MODE                RTC_MODE_I2C
#define RTC_I2C                 i2c0
#define RTC_I2C_SDA_PIN         4
#define RTC_I2C_SCL_PIN         5
#define RTC_I2C_FREQ            400 // in kHz


/* ---------- Nixie configs ---------- */
#define NIXIE_ENABLE            true
#define NIXIE_NUM_CHIPS         3
#define NIXIE_NUM_DISPLAYS      6

#define NIXIE_I2C               RTC_I2C
#define NIXIE_I2C_SDA_PIN       RTC_I2C_SDA_PIN
#define NIXIE_I2C_SCL_PIN       RTC_I2C_SCL_PIN

#define NIXIE_SEQUENCE_INTERVAL 10   // in min
#define NIXIE_SEQUENCE_STEPS    2000 // in ms


/* ---------- Segment configs ---------- */
#define SEGMENT_ENABLE          true
#define SEGMENT_NUM_DISPLAYS    4


/* ---------- Timecode configs ---------- */
#define TIMECODE_ENABLE         true

#define TIMECODE_DECODER_ENABLE true
#define TIMECODE_DECODER_PIN    27

#define TIMECODE_ENCODER_ENABLE true
#define TIMECODE_ENCODER_PIN    28

/* ---------- Button configs ---------- */
#define NUM_BUTTONS             3

// Buttons GPIOS
#define BUTTON_0_PIN            6
#define BUTTON_1_PIN            7
#define BUTTON_2_PIN            8

// Buttons IDs
#define MODE_BUTTON             0
#define PLAY_BUTTON             1
#define RESET_BUTTON            2


/* ---------- Other configs ---------- */
#define LED_PIN                 25
#define DEBUG_PIN               2


#endif