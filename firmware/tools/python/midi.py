import mido
from mido import Message


# PORT_NAME = "Nixie Clock"
PORT_NAME = "MidiView"


def main():

    ports = mido.get_output_names()
    print(ports)

    port = mido.open_output(PORT_NAME)

    msg = Message('note_on', note=60)
    print(msg)

    port.send(msg)













if __name__ == "__main__":
    main()