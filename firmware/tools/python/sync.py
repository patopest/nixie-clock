import time
from datetime import datetime
import argparse
import serial
import serial.tools.list_ports

# The clock's serial commands
set_datetime_cmd    = "set-datetime"
set_time_cmd        = "set-time"


def get_port():
    ports = serial.tools.list_ports.grep("/dev/cu.usbmodem", include_links=False)
    for port in ports:
        if ("Pico" in port.product):
            print("Autodetected: {}".format(port))
            return port.device



def main():
    parser = argparse.ArgumentParser(description="Interact with the Nixie Clock")
    s = parser.add_argument_group("Time sync method", "The method used to sync the clock")
    s.add_argument("--localtime", "-l", action="store_true", default=False, help="Use localtime to sync the clock")
    s.add_argument("--ntp", "-n", action="store_true", default=False, help="Use NTP to sync the clock")
    p = parser.add_argument_group("The serial port", "Serial port arguments")
    p.add_argument("--port", "-p", type=str, default=get_port(), help="The serial port to use (default %(default)s)")
    p.add_argument("--baudrate", "-b", type=int, default=115200, help="The serial port's baudrate (default %(default)s)")

    args = parser.parse_args()

    now = None
    now_str = ""
    port = None
    port_name = args.port

    if (port_name == None):
        port_name = get_port()

    try:
        port = serial.Serial(port=port_name, baudrate=args.baudrate, timeout=1)
    except Exception as e:
        print(e)
        exit(1)

    if (args.localtime):
        now = datetime.now()
        now_str = now.strftime("%a %d-%m-%y %H:%M:%S")
        print(now_str)

    elif (args.ntp):
        pass    
    else:
        print("No time sync method provided! Please provide one!")
        exit(1)

    # Creating the commands and send it on serial
    cmd = set_datetime_cmd + " " + now_str + "\n"
    cmd = cmd.encode("utf-8")

    print("Sending command: {}".format(cmd))
    port.write(cmd)
    print("Done {}".format(port.out_waiting))

    resp = port.read_until()
    print("received: {}".format(resp))













if __name__ == "__main__":
    main()