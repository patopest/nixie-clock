# Sync the clock
This is a tool in python to sync the clock over serial using different time sync methods.

## Methods
- Localtime  
Uses the host's localtime to send to the clock (python's `datetime.now()`)
- NTP  
***FUTURE***

## Usage
- Setup

```shell
make dev

source venv/bin/activate
```

- Run

```shell
python run.py -h #To see all available arguments
```
Note: If no serial port is provided, it will autodetect one.

