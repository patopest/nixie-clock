# Nixie-Clock
A clock built using a RaspberryPi Pico, a tiny-RTC module and IN-14 Nixie Tubes.

## Hardware
- RaspberryPi Pico board
- Tiny-RTC module (using DS3231 IC) connected via I2C
- 7-segment LED display (for development)
- IN-12 Nixie tubes (x6)

Hardware designed using Kicad 6.0


## Firmware
Built using [pico-sdk](https://github.com/raspberrypi/pico-sdk).  
Used a 2nd pico board as a `picoprobe` to flash and monitor.

### Dependencies and tools
For instructions on how to build and use these tools, please check the [Getting Started](https://datasheets.raspberrypi.com/pico/getting-started-with-pico.pdf) guide.  

- Setup

```shell
git submodule init
git submodule update

```
#### Required:
- [pico-sdk](https://github.com/raspberrypi/pico-sdk): obviously
- [libltc](https://github.com/x42/libltc): for timecode


#### Optional
- [openocd](https://github.com/raspberrypi/openocd): To flash using SVD (with picoprobe or RaspberryPi board).
- [picotool](https://github.com/raspberrypi/picotool): Cool tool to see some infos about .elf files.
- [picoprobe](https://github.com/raspberrypi/picoprobe): You can clone and build it or use a .uf2 provided by RaspberryPi.
- [pico-examples](https://github.com/raspberrypi/pico-examples): SDK example apps (useful when developing).


### Main code

- Setup

```shell
# In the "firmware" directory
mkdir build && cd build
export PICO_SDK_PATH="../pico-sdk"
cmake ..
```

- Build

```shell
make -j4
```

- Flash

Using the custom built `openocd` and a `picoprobe` board as SWD flasher and monitor.

```shell
make flash
```

## License

Firmware and Software: LGPL v3

Hardware: CERN-OHL-W v2


## Docs
- [Getting Started with RaspberryPi Pico](https://datasheets.raspberrypi.com/pico/getting-started-with-pico.pdf)
- [SDK documentation as PDF](https://datasheets.raspberrypi.com/pico/raspberry-pi-pico-c-sdk.pdf)
- [SDK documentation on Web](https://www.raspberrypi.com/documentation/pico-sdk/)
- [Pico datasheet](https://datasheets.raspberrypi.com/pico/pico-datasheet.pdf)
- [DS1307 tiny RTC issues forum thread](https://forum.arduino.cc/t/tiny-rtc-i2c-module-issue/173162/48)
Had to desolder a couple of resistors to make it work with a CR2032.
- [DS3231 calibration](https://github.com/SergejBre/SynchroTime)
Inspiration for how to calibrate the time drift on DS3231 modules.
- [USB in a nutshell](https://www.beyondlogic.org/usbnutshell/usb5.shtml)
- [MIDI specifications](https://www.midi.org/specifications/midi1-specifications)
